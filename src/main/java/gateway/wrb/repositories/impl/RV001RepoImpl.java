package gateway.wrb.repositories.impl;

import gateway.wrb.domain.RV001Info;
import gateway.wrb.domain.RV001SInfo;
import gateway.wrb.domain.VLR001SInfo;
import gateway.wrb.model.RV001DTO;
import gateway.wrb.model.VLRV001DTO;
import gateway.wrb.repositories.RV001Repo;
import gateway.wrb.util.Validator;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


@Repository
@Transactional
public class RV001RepoImpl implements RV001Repo {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<RV001Info> getAllRV001() {
        String hql = " FROM RV001Info as u ORDER BY u.id";
        return (List<RV001Info>) entityManager.createQuery(hql).getResultList();
    }

    @Override
    public void addRV001S(RV001SInfo rv001SInfo) {
        entityManager.persist(rv001SInfo);
    }

    @Override
    public List<RV001DTO> filterRV001(String orgCd, String bankCd, String bankCoNo, String outActNo, String bankRsvSdt, String bankRsvEdt) {
        List<RV001DTO> rv001DTOS = new ArrayList<>();
        Map<String, String> mapParam = new LinkedHashMap<>();
        StringBuilder hql = new StringBuilder("FROM RV001Info as rv001 ");
        if (Validator.validateString(bankCoNo)) {
            mapParam.put("bankCoNo", bankCoNo);
            hql.append(" INNER JOIN FbkFilesInfo AS fbkFile ON rv001.fbkname = fbkFile.fbkname WHERE fbkFile.conos = :bankCoNo ");
        }
        if (Validator.validateString(outActNo)) {
            mapParam.put("outActNo", outActNo);
            hql.append(" AND rv001.rcvacno = :outActNo");
        }
        if (Validator.validateString(bankRsvSdt)) {
            mapParam.put("bankRsvSdt", bankRsvSdt);
            hql.append(" AND STR_TO_DATE(rv001.trndt,'%Y%m%d')  > STR_TO_DATE(:bankRsvSdt,'%Y%m%d') ");
        }
        if (Validator.validateString(bankRsvEdt)) {
            mapParam.put("bankRsvEdt", bankRsvEdt);
            hql.append(" AND STR_TO_DATE(rv001.trndt,'%Y%m%d')  < STR_TO_DATE(:bankRsvEdt,'%Y%m%d') ");
        }
        Query query = entityManager.createQuery(hql.toString());
        for (Map.Entry<String, String> entry : mapParam.entrySet()) {
            query.setParameter(entry.getKey(), entry.getValue());
        }

        List<?> rs = new ArrayList<>();
        rs = query.getResultList();
        for (int i = 0; i < rs.size(); ++i) {
            Object[] row = (Object[]) rs.get(i);
            RV001DTO rv001DTO = new RV001DTO();
            rv001DTO = castToDTO((RV001Info) row[0]);
            rv001DTOS.add(rv001DTO);
        }
        return rv001DTOS;
    }

    @Override
    public void addRV001(RV001Info rv001Info) {
        entityManager.persist(rv001Info);
    }

    /*Đạt sửa*/
    @Override
    public void updateRV001(RV001Info rv001Info) {

        entityManager.merge(rv001Info);
    }

    @Override
    public void deleteRV001(RV001Info rv001Info) {
        entityManager.detach(rv001Info);
    }

    /* Đạt sửa lại isRV001Exist */
    @Override
    public boolean isRV001Exist(String msgDscd, String trnDt, String trnTm, String msgNo,
                                String wdracno, String rcvacNo, String wdram) {
        Map<String, String> mapParam = new LinkedHashMap<>();
        String hql = "FROM RV001Info as rv001  WHERE";
        if (Validator.validateString(msgDscd)) {
            mapParam.put("msgDscd", msgDscd);
            hql = hql.concat("  rv001.msgdscd = :msgDscd AND");
        }
        if (Validator.validateString(trnDt)) {
            mapParam.put("trnDt", trnDt);
            hql = hql.concat(" rv001.trndt = :trnDt AND");
        }
        if (Validator.validateString(trnTm)) {
            mapParam.put("trnTm", trnTm);
            hql = hql.concat(" rv001.trntm = :trnTm AND");
        }
        if (Validator.validateString(msgNo)) {
            mapParam.put("msgNo", msgNo);
            hql = hql.concat(" rv001.msgno = :msgNo AND");
        }
        if (Validator.validateString(wdracno)) {
            mapParam.put("wdracno", wdracno);
            hql = hql.concat(" rv001.wdracno = :wdracno AND");
        }
        if (Validator.validateString(rcvacNo)) {
            mapParam.put("rcvacNo", rcvacNo);
            hql = hql.concat(" rv001.rcvacno = :rcvacNo AND");
        }
        if (Validator.validateString(wdram)) {
            mapParam.put("wdram", wdram);
            hql = hql.concat(" rv001.wdram = :wdram");
        }
        if (hql.endsWith("WHERE")) {
            hql = hql.replace("WHERE", "");
        }
        if (hql.endsWith("AND")) {
            hql = hql.substring(0, hql.lastIndexOf("AND") - 1);
        }
        Query query = entityManager.createQuery(hql);
        for (Map.Entry<String, String> param : mapParam.entrySet()) {
            query.setParameter(param.getKey(), param.getValue());
        }
        Integer count = query.getResultList().size();
        return count > 0 ? true : false;
    }


    private RV001DTO castToDTO(RV001Info rv001Info) {
        RV001DTO rv001DTO = new RV001DTO();
        rv001DTO.setMsgno(rv001Info.getMsgno());
        rv001DTO.setRcvacno(rv001Info.getRcvacno());
        rv001DTO.setTrndt(rv001Info.getTrndt());
        rv001DTO.setTrntm(rv001Info.getTrntm());
        rv001DTO.setWdracno(rv001Info.getWdracno());
        rv001DTO.setWdram(rv001Info.getWdram());
        rv001DTO.setCantrancnte(rv001Info.getCantrancnte());
        rv001DTO.setCantrantotamte(rv001Info.getCantrantotamte());
        rv001DTO.setCurcd(rv001Info.getCurcd());
        rv001DTO.setIncdaccgb(rv001Info.getIncdaccgb());
        rv001DTO.setIstdscd(rv001Info.getIstdscd());
        rv001DTO.setNortrancnte(rv001Info.getNortrancnte());
        rv001DTO.setNortrantotamte(rv001Info.getNortrantotamte());
        rv001DTO.setOrgcantrancnte(rv001Info.getOrgcantrancnte());
        rv001DTO.setOrgcantrantotamte(rv001Info.getOrgcantrantotamte());
        rv001DTO.setRcvacdppenm(rv001Info.getRcvacdppenm());
        rv001DTO.setRcvbk1cd(rv001Info.getRcvbk1cd());
        rv001DTO.setRcvbk2cd(rv001Info.getRcvbk2cd());
        rv001DTO.setRcvviracno(rv001Info.getRcvviracno());
        rv001DTO.setRefno(rv001Info.getRefno());
        rv001DTO.setStsdscd(rv001Info.getStsdscd());
        rv001DTO.setRegmodcd(rv001Info.getRegmodcd());
        rv001DTO.setTobkdscd(rv001Info.getTobkdscd());
        rv001DTO.setTrnsrno(rv001Info.getTrnsrno());
        rv001DTO.setTrnstat(rv001Info.getTrnstat());
        rv001DTO.setVractcusnm(rv001Info.getVractcusnm());
        rv001DTO.setWdrviracno(rv001Info.getWdrviracno());
        return rv001DTO;
    }
}

